# docker-magento2

Modified based of https://hub.docker.com/r/sensson/magento2
Add supports for php8 with xdebug, magento 2.4.4, elasticsearch

# Usage:
1) After clone this source, Create a folder for magento2 source code in the base directory:
	mkdir magento2

2) Unzip magento 2.4.4 source into magento2 folder
	You should be able find files such as magento2/bin/magento

3) build and run the docker env command:
  	docker compose up --build

# Persistent storage

The container assumes you do not store data in a folder along with the
application. Don't use Docker volumes for scale. Use CephFS, GlusterFS or
integrate with S3 or S3-compatible services such as [Fuga.io](https://fuga.io).

# Configuration

## Environment variables

Environment variable  | Description                   | Default
--------------------  | -----------                   | -------
MYSQL_HOSTNAME        | MySQL hostname                | mysql
MYSQL_USERNAME        | MySQL username                | root
MYSQL_PASSWORD        | MySQL password                | secure
MYSQL_DATABASE        | MySQL database                | magento
CRYPTO_KEY            | Magento Encryption key        | Emtpy
URI                   | Uri (e.g. http://localhost)   | http://localhost
RUNTYPE               | Set to development to enable  | Empty
ADMIN_USERNAME        | Administrator username        | admin
ADMIN_PASSWORD        | Administrator password        | adm1nistrator
ADMIN_FIRSTNAME       | Administrator first name      | admin
ADMIN_LASTNAME        | Administrator last name       | admin
ADMIN_EMAIL           | Administrator email address   | admin@localhost.com
BACKEND_FRONTNAME     | The URI of the admin panel    | admin
CURRENCY              | Magento's default currency    | EUR
LANGUAGE              | Magento's default language    | en_US
CONTENT_LANGUAGES     | The languages used in Magento | en_US
TIMEZONE              | Magento's timezone            | Europe/Amsterdam
CRON                  | Run as a cron container       | false
UNATTENDED            | Run unattended upgrades       | false
ELASTICSEARCH_*

Include the port mapping in `URI` if you run your shop on a local development
environment, e.g. `http://localhost:8000/`.

## Hooks

Hooks allow you to run certain actions at certain points in the startup
procedure without having to rewrite `entrypoint.sh`. You can use hooks to
import data, preconfigure settings or run other scripts that need to run
each startup sequence.

Hook point   | Location                 | Description
----------   | ---------                | -----------
Pre install  | `/hooks/pre_install.sh`  | Runs before an installation or update.
Pre compile  | `/hooks/pre_compile.sh`  | Runs before code compilation starts.
Post install | `/hooks/post_install.sh` | Runs after Magento has been installed.

You need to `COPY` any hooks to this location yourself.

## Development mode

Setting `RUNTYPE` to `development` will turn on public error reports. Anything
else will leave it off. It will also set `display_errors` to on in PHP. This is
set to off by default.

A basic `docker-compose.yml` has been provided to help during the development
of this image.
